from selenium import webdriver
from selenium.webdriver.support.ui import Select

INFO_PAGE = "https://www.reservauto.net/Scripts/Client/PersonalInformation.asp?CurrentLanguageID=1"


class Connector:
    def __init__(self, password, user):
        self.br = webdriver.Chrome()
        self.br.get("https://www.communauto.com/fr/connexion.html")
        self.br.find_element_by_id("Username").send_keys(user)
        self.br.find_element_by_id("Password").send_keys(password)
        self.br.find_element_by_id("send").click()

    def get_invoice_list(self):
        self.br.get(INFO_PAGE)
        select = Select(self.br.find_element_by_id('InvoiceNo'))
        l = [o.text.encode("utf-8") for o in select.options]
        return l[1:]

    def get_invoice(self, option_name):
        self.br.get(INFO_PAGE)
        select = Select(self.br.find_element_by_id('InvoiceNo'))
        for option in select.options:
            if option.text == option_name:
                option.click()
