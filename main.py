#!/usr/bin/env python
import communautoConnector
import communautoPdfParser
import csv
import dateparser

UPATH = "HOME_PATH"
DLPATH = UPATH+"/Downloads/"
PSWD = ""
USER = ""

if __name__ == "__main__":
    c = communautoConnector.Connector(PSWD, USER)
    l = c.get_invoice_list()
    for i in l:
       c.get_invoice(i)

    pdf2txt = communautoPdfParser.PdfToText(DLPATH, UPATH+"/pdf2txt/")
    pdf2txt.process()

    parser = communautoPdfParser.Parser(UPATH+"/pdf2txt/")
    l = parser.parse_files()
    with open('factures.csv', "w+") as f:
        writer = csv.writer(f, dialect='excel')
        for r in l:
            try:
                writer.writerow((r[0], dateparser.parse(r[1]['date'][8:]).date(), float((r[2]['solde']).replace(",", "."))))
            except:
                print r
