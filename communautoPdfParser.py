import os
import re
import subprocess

LANG = [
    {'regex': re.compile(r'Nouveau solde :.* '), "regex2": re.compile(r'-*\d+,\d+'),"token_t": "solde"},
    {'regex': re.compile(r'du'), "regex2": re.compile(r'\d+ au \d+ \w+ \d+'), "token_t": "date"},
]


class PdfToText:

    def __init__(self, ifolder, ofolder):
        self.in_folder = ifolder
        self.out_folder = ofolder

    def __execute_pdftotext__(self, pdf_file, txt_file):
        subprocess.call(["pdftotext", "-layout", "-enc", "UTF-8", pdf_file, txt_file])

    def process(self):
        file_name_list = []
        for file in os.listdir(self.in_folder):
            new_file = file.replace(".pdf", ".txt")
            file_name_list.append(new_file)
            self.__execute_pdftotext__(self.in_folder + file, self.out_folder + new_file)
        return file_name_list


class Parser:

    def __init__(self, txt_folder):
        self.text_folder_dir = txt_folder

    def parse_files(self):
        parsed_value = []
        for file in os.listdir(self.text_folder_dir):
            parsed_value.append(self.parse_file(file, LANG))
        return parsed_value

    def parse_file(self, file, lang):
        values = [file.replace(".txt", "")]
        with open(self.text_folder_dir + file, 'r') as f:
            data = f.read()
            for phrase in data.split('\n'):
                value = self.__parse_phrase__(phrase, lang)
                if value:
                    values.append(value)
        return values

    def __parse_phrase__(self, phrase, lang):
        d = {}
        for l in lang:
            if self.__find__(l["regex"], phrase):
                finding = self.__find__(l["regex2"], phrase)
                if finding:
                    d[l["token_t"]] = finding
        if len(d.get(lang[0]["token_t"], "")) > 0 or len(d.get(lang[1]["token_t"], "")) > 0:
            return d
        else:
            return None

    def __find__(self, regex, text):
        r = regex.search(text)
        if r and r.group():
            return r.group()
        else:
            return None



